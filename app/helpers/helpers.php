<?php
/* CONSTANT */
define("INSERT_SUCCESSFULLY_MESSAGE", "# added successfully.");
define("UPDATE_SUCCESSFULLY_MESSAGE", "# updated successfully.");
define("DELETE_SUCCESSFULLY_MESSAGE", "# deleted successfully");
define("SENDING_SUCCESSFULLY_MESSAGE", "# send successfully");

define("INVETORY_HAS_BEEN_REGISTERED_MESSAGE", "The # has been assigned to this *");

/* METHOD */
function validationErrorsToString($errArray) {
    $valArr = array();
    foreach ($errArray->toArray() as $key => $value) {
        $errStr = $value[0];
        array_push($valArr, $errStr);
    }
    if(!empty($valArr)){
        $errStrFinal = implode("\n", $valArr);
    }
    return $errStrFinal;
}

function setActiveLeftNavigation(){

    $path = Request::path();

    /* ROOM */
    $is_hotel_room = strpos( $path, "room/" ) !== false? true:false;

    /* SETTING */
    $is_setting = false;
    $is_setting = strpos( $path, "setting/" ) !== false? true:false;

    $is_setting_inventory = false;
    if( $is_setting ){
         $is_setting_inventory = strpos( $path, "/inventory" ) !== false? true:false;
    }

    $arr_menus = array(
                    "path"=>$path,
                    "is_hotel_room"=>$is_hotel_room,
                    "is_setting"=>$is_setting,
                    "is_setting_inventory"=>$is_setting_inventory
                );

    return $arr_menus;
}

function moneyFormat($money){
  return "IDR ".number_format($money,0,",",",");
}

function getInventoryHasBeenRegisteredMessage($inventory, $type ){
  $msg = str_replace("#", $inventory, INVETORY_HAS_BEEN_REGISTERED_MESSAGE);
  $msg = str_replace("*", $type, $msg);

  return $msg;
}

function reportDateToMySql($dt){
    $result = '';
    $arr_dt = explode("/", $dt);
    return $arr_dt[2].'-'.$arr_dt[0].'-'.$arr_dt['1'];
}

function slug($text)
        {
            // megubah karakter non huruf dengan strip
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
            // trim
            $text = trim($text, '-');
            // transliterate
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            // ubah semua huruf menjadi huruf kecil
            $text = strtolower($text);
            // hapus karakter yang tidak diinginkan
            $text = preg_replace('~[^-\w]+~', '', $text);
            if (empty($text))
            {
                return 'n-a';
            }
                return $text;
        }
