<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SuperAdmin extends Authenticatable
{
    use Notifiable;
    protected $table = 'superadmin';
    protected $guard = 'superadmin';

    protected $fillable = [
        'name', 'email', 'password','is_admin'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
