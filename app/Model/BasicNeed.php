<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BasicNeed extends Model
{
    protected $table = 'basic_needs';
}
