<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContactAdmin extends Model
{
    protected $table = 'contact';
}
