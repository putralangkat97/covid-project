<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 
use Yajra\Datatables\Datatables;
use App\Model\StoreAdmin;
use Validator,Redirect,Response,File;
use Illuminate\Support\Facades\Hash;
use Stevebauman\Location\Facades\Location;
use Auth;
use Carbon\Carbon;
use App\Model\BasicNeed;

class UserController extends Controller
{
    public function index(){
    	return view('user.index');
    }

    public function storeIndex(){
       $data = StoreAdmin::paginate(9);
       return view('user.store',compact('data'));
   }

    // Food
    // Rice
   public function riceDetail(){
       if (request()->ajax()) {
        return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Rice')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
        ->addIndexColumn()
        ->addColumn('action', function ($row) {
            $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
    return view('user.foods.rice');
}
// Cooking Oil
public function oilDetail(){
    if (request()->ajax()) {
     return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Cooking Oil')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
     ->addIndexColumn()
     ->addColumn('action', function ($row) {
         $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
         return $btn;
     })
     ->rawColumns(['action'])
     ->make(true);
 }
 return view('user.foods.cookingoil');
}
// Sugar
public function sugarDetail(){
    if (request()->ajax()) {
     return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Sugar')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
     ->addIndexColumn()
     ->addColumn('action', function ($row) {
         $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
         return $btn;
     })
     ->rawColumns(['action'])
     ->make(true);
 }
 return view('user.foods.sugar');
}
// Quota
public function quotaDetail(){
    if (request()->ajax()) {
     return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Internet Quota')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
     ->addIndexColumn()
     ->addColumn('action', function ($row) {
         $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
         return $btn;
     })
     ->rawColumns(['action'])
     ->make(true);
 }
 return view('user.foods.quota');
}
// Meat
public function meatDetail(){
    if (request()->ajax()) {
     return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Meat')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
     ->addIndexColumn()
     ->addColumn('action', function ($row) {
         $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
         return $btn;
     })
     ->rawColumns(['action'])
     ->make(true);
 }
 return view('user.foods.meat');
}
// Milk
public function milkDetail(){
    if (request()->ajax()) {
     return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Milk')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
     ->addIndexColumn()
     ->addColumn('action', function ($row) {
         $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
         return $btn;
     })
     ->rawColumns(['action'])
     ->make(true);
 }
 return view('user.foods.milk');
}
// Salt
public function saltDetail(){
    if (request()->ajax()) {
     return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Salt')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
     ->addIndexColumn()
     ->addColumn('action', function ($row) {
         $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
         return $btn;
     })
     ->rawColumns(['action'])
     ->make(true);
 }
 return view('user.foods.salt');
}
// Eggs
public function eggsDetail(){
    if (request()->ajax()) {
     return datatables()->of(BasicNeed::join('admin','basic_needs.id_store','=','admin.id')->where('basic_needs.type','=','Eggs')->select('admin.id','admin.name','basic_needs.item','basic_needs.price','basic_needs.description','basic_needs.value' ))
     ->addIndexColumn()
     ->addColumn('action', function ($row) {
         $btn = ' <a href="/detail/store/'.$row->id.'"  data-toggle="tooltip" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
         return $btn;
     })
     ->rawColumns(['action'])
     ->make(true);
 }
 return view('user.foods.eggs');
}


// End Food

public function storeDetail($id){
    $data = StoreAdmin::where('id','=',$id)->get();        
    return view('user.storeDetail',compact('data'));
}

public function messageSend(Request $request){
    $validator = Validator::make(request()->all(), [
        'name' => 'required',
        'email'     => 'required',
        'phone'    => 'required|max:12',
        'description' => 'required',

    ]);

    if ($validator->fails()) {
        $result = validationErrorsToString($validator->errors());
        return response()
        ->json(['status' => false, 'description' =>  $result]); 
    }else{
        DB::table('contact')->insert([
            'id_store' => $request->id,
            'name' => $request->name,
            'description' => $request->description,
            'email' => $request->email,
            'phone' => $request->phone,
            'send_time' => Carbon::now('Asia/Jakarta'),
            'status' => 'N',

        ]);

        return response()
        ->json(['status' => true, 'description' =>  str_replace("#", 'Message', SENDING_SUCCESSFULLY_MESSAGE)]);
    }

}

public function showData($id){
    $id_store = $id;
  if (request()->ajax()) {
    return datatables()->of(BasicNeed::select('*')->where('id_store','=',$id_store))
    ->addIndexColumn()
    ->make(true);
}
return view('user.storeDetail');
}


public function aboutUs(){
    return view('user.aboutus');
}

public function contactUs(){
    return view('user.contactus');
}

public function userSend(Request $request){
    $validator = Validator::make(request()->all(), [
        'name' => 'required',
        'email'     => 'required',
        'phone'    => 'required|max:12',
        'message' => 'required',

    ]);

    if ($validator->fails()) {
        $result = validationErrorsToString($validator->errors());
        return response()
        ->json(['status' => false, 'description' =>  $result]); 
    }else{
        DB::table('contact_admin')->insert([
            'name' => $request->name,
            'description' => $request->message,
            'email' => $request->email,
            'phone' => $request->phone,
            'send_time' => Carbon::now('Asia/Jakarta'),
            'status' => 'N',

        ]);

        return response()
        ->json(['status' => true, 'description' =>  str_replace("#", 'Message', SENDING_SUCCESSFULLY_MESSAGE)]);
    }
}

}


