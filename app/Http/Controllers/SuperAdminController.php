<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 
use Yajra\Datatables\Datatables;
use App\Model\StoreAdmin;
use Validator,Redirect,Response,File;
use Illuminate\Support\Facades\Hash;
use Stevebauman\Location\Facades\Location;
use Auth;
use App\helpers\UserSystemInfoHelper;
use Mail;
use App\Model\ContactSuperAdmin;

class SuperAdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth:superadmin');
    }
    public function dashboard(){
        return view('admin.dashboard');
    }

    public function storeindex(){
        if(request()->ajax()) {
            return datatables()->of(StoreAdmin::select('*')->orderByRaw('is_active ASC'))
            ->addIndexColumn() 
            ->addColumn('action', function($row){ 
                $btn = '
                <div class="list-icons">
                <div class="dropdown">
                <a href="#" class="list-icons-item" data-toggle="dropdown">
                <i class="fas fa-bars"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                <a href="/store/detail/'.$row->id.'" class="dropdown-item detailStore" data-id="'.$row->id.'"><i class="fas fa-eye text-secondary"></i>&emsp;View</a>
                <a href="javascript:void(0)" class="dropdown-item updateStore" data-id="'.$row->id.'" data-toggle="modal"><i class="fas fa-pencil-alt text-warning"></i>&emsp;Update</a>
                <a href="javascript:void(0)" class="dropdown-item deleteStore" data-id="'.$row->id.'" data-toggle="modals" data-original-title="Delete"><i class="fas fa-trash-alt text-danger"></i>&emsp;Delete</a>
                </div>
                </div>
                </div>
                ';   
                return $btn; 
            }) 
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('admin.store');
    }

    public function storeAdd(Request $request)
    {
       $validator = Validator::make(request()->all(), [
            'store_name' => 'required',
            'Address'     => 'required',
            'Phone_Number'=> 'required|max:12',
            'Password'    => 'required|min:8',
            'Email'       => 'required|',
            'open_time'        => 'required',
            'closed_time'      => 'required', 
            'gambar'       => 'required|max:100',

        ]);

        if ($validator->fails()) {
            $result = validationErrorsToString($validator->errors());
            return response()
            ->json(['status' => false, 'description' =>  $result]); 
        }else{
            if ($request->id == '') {
                DB::table('admin')->insert([
                    'name' => $request->store_name,
                    'image' => $request->gambar,
                    'address' => $request->Address,
                    'phone' => $request->Phone_Number,
                    'password' => Hash::make($request->Password),
                    'email' => $request->Email,
                    'open_time' => $request->open_time,
                    'description' => null,
                    'closed_time' => $request->closed_time,
                    'is_active' => 'N',
                    

                ]);

                return response()
                ->json(['status' => true, 'description' =>  str_replace("#", 'Store', INSERT_SUCCESSFULLY_MESSAGE)]);
            }else{
            // Delete Preivous Image
                $data= DB::table('admin')   
                ->where('id',$request->id)->get(); 
                if($request->gambar != $data[0]->image){
                    if($data[0]->image!=null){ 
                        if(file_exists(public_path().'/public/storeLogo/'.$data[0]->image)){
                            unlink(public_path().'/public/storeLogo/'.$data[0]->image);
                        }
                    } 
                } 

                DB::table('admin')
                ->where('id',$request->id)
                ->update(
                    [
                        'name' => $request->store_name, 
                        'image' => $request->gambar,
                        'address' => $request->Address,
                        'phone' => $request->Phone_Number,
                        'password' => Hash::make($request->Password),
                        'email' => $request->Email,
                        'open_time' => $request->open_time,
                        'closed_time' => $request->closed_time,
                        'is_active' => 'N',
                    ]);
                return response()
                ->json(['status' => true, 'description' =>  str_replace("#", 'Store', UPDATE_SUCCESSFULLY_MESSAGE)]);     

            }


        }
    }
    public function uploadImage(Request $request){
        $validator = Validator::make($request->all(),
            [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
        );
        if ($validator->fails()){
            return array(
                'fail' => true,
                'errors' => $validator->errors()
            );
        }

        $extension = $request->file('image')->getClientOriginalExtension();
        $dir = 'public/storeLogo/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('image')->move($dir, $filename); 
        return $filename;
    }

    public function storeEdit($id)
    {
        $data = DB::table('admin')->where('id',$id)->get();
        return response()->json($data);
    }

    public function storeDetail($id)
    {
        $data = DB::table('admin')->where('id',$id)->get();
        return view('admin.storeDetail',compact('data'));
    }
    
    public function deleteStore($id){
        DB::table('admin')->where('id', $id)
        ->delete();
        return response()
        ->json(['status' => true, 'description' =>  str_replace("#", 'Store',DELETE_SUCCESSFULLY_MESSAGE)]);

    }

    public function storeLog(){
        $item = [
            'ip'      =>   $ipinfo = UserSystemInfoHelper::get_ip(),
            'browser' =>   $getbrowser = UserSystemInfoHelper::get_browsers(),
            'os'      =>   $getos = UserSystemInfoHelper::get_os(),
            'location' =>  $location = Location::get('id'),
        ];
        return view('admin.storeLog',$item);
    }

    public function contactUs(){
        if (request()->ajax()) {
            return datatables()->of(ContactSuperAdmin::select('*')->orderByRaw('status DESC'))
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '
                <div class="list-icons">
                <div class="dropdown">
                <a href="#" class="list-icons-item" data-toggle="dropdown">
                <i class="fas fa-bars"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                <a href="/admin/detailContact/'.$row->id.'" class="dropdown-item detailMessage" data-id="'.$row->id.'"><i class="fas fa-eye text-secondary"></i>&emsp;View</a>
                <a href="javascript:void(0)" class="dropdown-item deleteEmail" data-id="' . $row->id . '" data-toggle="modals" data-original-title="Delete"><i class="fas fa-trash-alt text-danger"></i>&emsp;Delete</a>
                </div>
                </div>
                </div>
                ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.contactUs');
    } 

    public function detailContact($id){
        DB::table('contact_admin')->where('id',$id)->update([
            'status' => 'Y',
        ]);
        $data = DB::table('contact_admin')->where('id',$id)->get();

        return view('admin.contactDetail',compact('data'));
    }

    public function adminSend(Request $request){
        $name = Auth::user()->name;
        $validator = Validator::make(request()->all(), [
            'user_name' => 'required',
            'user_email'     => 'required',
            'message'=> 'required',
            'your_email'    => 'required',

        ]);

        if ($validator->fails()) {
            $result = validationErrorsToString($validator->errors());
            return response()
            ->json(['status' => false, 'description' =>  $result]); 
        }else{
                Mail::send('store.email', ['name' => $request->user_name,'name_admin' => $name, 'description' => $request->message,'email' => $request->user_email ], function ($message) use ($request,$name)
                {
                    $message->subject('CekSembakoAjaa Contact');
                    $message->from($request->your_email,$name);
                    $message->to($request->user_email);
                });
                return response()
                ->json(['status' => true, 'description' =>  str_replace("#", 'Message', SENDING_SUCCESSFULLY_MESSAGE)]);  
            
            

            
        }
    }

    public function adminDelete($id){
         DB::table('contact_admin')->where('id', $id)
        ->delete();
        return response()
        ->json(['status' => true, 'description' =>  str_replace("#", 'Message',DELETE_SUCCESSFULLY_MESSAGE)]);
    }
}

