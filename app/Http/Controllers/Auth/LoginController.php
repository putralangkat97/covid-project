<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function show()
    {
        if(Auth::guard('superadmin')->check()){
            return redirect('/dashboard');
        }
            else if(Auth::guard('admin')->check()){
                return redirect('store/dashboard');
          }
        return view('auth.login');
    }

    public function login(Request $request)
    {   
        if($request->level=='superadmin'){
            if (auth()->guard('superadmin')->attempt(
              ['email'    => $request->email, 
              'password' => $request->password,
              
            ]))
              
            {
              date_default_timezone_set('Asia/Jakarta');
              
              DB::table('superadmin')->where('id', Auth::guard('superadmin')->user()->id)->update([
                'updated_at' => date('Y-m-d H:i:s'),
              ]);
              return redirect()->route('dashboard');
            } 
          }
          if($request->level=='admin'){
          date_default_timezone_set('Asia/Jakarta');  
            if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password ])) {
                DB::table('admin')
                ->where('email',$request->email)
                ->where('id', Auth::guard('admin')->user()->id)
                ->update(['is_active'  => 'Y',
                  'updated_at' => date('Y-m-d H:i:s')]); 
                return redirect()->route('admin.dashboard');
            } 
        }
          return back()->withErrors(['email' => 'Email or password are wrong.']);

    }

    public function logout()
    { 
      if (Auth::guard('superadmin')->check()) {
        DB::table('superadmin')->where('id', Auth::guard('superadmin')->user()->id)->update([
          'updated_at' => date('Y-m-d H:i:s'),
        ]);
        Auth::guard('superadmin')->logout();
      } 
      if (Auth::guard('admin')->check()) {

        DB::table('admin')
            ->where('id',Auth::guard('admin')->user()->id)
            ->update(['is_active'  => 'N',
              'updated_at' => date('Y-m-d H:i:s')]); 
        Auth::guard('admin')->logout();
      } 

      return redirect('login');
    }
}
