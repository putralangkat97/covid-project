<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\StoreAdmin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\BasicNeed;
use Auth;
use App\Model\ContactAdmin;
use Mail;

class StoreAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function dashboard()
    {
        $data = DB::table('admin')->where('id', Auth::user()->id)->get();
        return view('store.dashboard', compact('data'));
    }

    public function editStore($id)
    {
        $data = StoreAdmin::where('id', $id)->get();
        return response()->json($data);
    }

    public function updateStore(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'store_name' => 'required',
            'Address'     => 'required',
            'Phone_Number' => 'required|max:12',
            'Email'       => 'required|',
            'open_time'        => 'required',
            'closed_time'      => 'required',
            'gambar'       => 'required|max:100',
            'description' => 'required'

        ]);

        if ($validator->fails()) {
            $result = validationErrorsToString($validator->errors());
            return response()
                ->json(['status' => false, 'description' =>  $result]);
        } else {
            // Delete Preivous Image
            $data = DB::table('admin')
                ->where('id', $request->id)->get();
            if ($request->gambar != $data[0]->image) {
                if ($data[0]->image != null) {
                    if (file_exists(public_path() . '/public/storeLogo/' . $data[0]->image)) {
                        unlink(public_path() . '/public/storeLogo/' . $data[0]->image);
                    }
                }
            }

            DB::table('admin')
                ->where('id', $request->id)
                ->update(
                    [
                        'name' => $request->store_name,
                        'image' => $request->gambar,
                        'address' => $request->Address,
                        'phone' => $request->Phone_Number,
                        'email' => $request->Email,
                        'open_time' => $request->open_time,
                        'description' => $request->description,
                        'closed_time' => $request->closed_time,
                        'is_active' => 'N',
                    ]
                );
            return response()
                ->json(['status' => true, 'description' =>  str_replace("#", 'Store', UPDATE_SUCCESSFULLY_MESSAGE)]);
        }
    }

    public function uploadImage(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
        );
        if ($validator->fails()) {
            return array(
                'fail' => true,
                'errors' => $validator->errors()
            );
        }

        $extension = $request->file('image')->getClientOriginalExtension();
        $dir = 'public/storeLogo/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('image')->move($dir, $filename);
        return $filename;
    }

    public function basicNeed()
    {
		$id = Auth::user()->id;     

        if (request()->ajax()) {
            return datatables()->of(BasicNeed::select('*')->where('id_store','=',$id))
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '
                <div class="list-icons">
                <div class="dropdown">
                <a href="#" class="list-icons-item" data-toggle="dropdown">
                <i class="fas fa-bars"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                <a href="javascript:void(0)" class="dropdown-item updateItem" data-id="' . $row->id . '" data-toggle="modal"><i class="fas fa-pencil-alt text-warning"></i>&emsp;Update</a>
                <a href="javascript:void(0)" class="dropdown-item deleteItem" data-id="' . $row->id . '" data-toggle="modals" data-original-title="Delete"><i class="fas fa-trash-alt text-danger"></i>&emsp;Delete</a>
                </div>
                </div>
                </div>
                ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('store.basicNeed');
    }

    public function itemAdd(Request $request){
        $id = Auth::user()->id;
        $validator = Validator::make(request()->all(), [
            'item_name' => 'required',
            'type'     => 'required',
            'description'=> 'required',
            'value'    => 'required|integer',
            'price'       => 'required|integer',

        ]);

        if ($validator->fails()) {
            $result = validationErrorsToString($validator->errors());
            return response()
            ->json(['status' => false, 'description' =>  $result]); 
        }else{
            if ($request->id == '') {
                DB::table('basic_needs')->insert([
                    'id_store' => $id,
                    'item' => $request->item_name,
                    'description' => $request->description,
                    'price' => $request->price,
                    'value' => $request->value,
                    'type' => $request->type
                    
                    ]);

                return response()
                ->json(['status' => true, 'description' =>  str_replace("#", 'Item', INSERT_SUCCESSFULLY_MESSAGE)]);
            }else{
            

                DB::table('basic_needs')
                ->where('id',$request->id)
                ->update(
                    [
                        'item' => $request->item_name, 
                        'type' => $request->type,
                        'value' => $request->value,
                        'price' => $request->price,
                        'description' => $request->description,
                    ]);
                return response()
                ->json(['status' => true, 'description' =>  str_replace("#", 'Item', UPDATE_SUCCESSFULLY_MESSAGE)]);     

            }


        }
    }

    public function itemUpdate($id){
        $data = DB::table('basic_needs')->where('id',$id)->get();

        return response()->json($data);
    }

    public function itemDelete($id)
    {
        DB::table('basic_needs')->where('id', $id)
        ->delete();
        return response()
        ->json(['status' => true, 'description' =>  str_replace("#", 'Item',DELETE_SUCCESSFULLY_MESSAGE)]);

    }

    public function contactIndex(){
        $id = Auth::user()->id;     

        if (request()->ajax()) {
            return datatables()->of(ContactAdmin::select('*')->where('id_store','=',$id)->orderByRaw('status DESC'))
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '
                <div class="list-icons">
                <div class="dropdown">
                <a href="#" class="list-icons-item" data-toggle="dropdown">
                <i class="fas fa-bars"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                <a href="/store/detailContact/'.$row->id.'" class="dropdown-item detailMessage" data-id="'.$row->id.'"><i class="fas fa-eye text-secondary"></i>&emsp;View</a>
                <a href="javascript:void(0)" class="dropdown-item deleteEmail" data-id="' . $row->id . '" data-toggle="modals" data-original-title="Delete"><i class="fas fa-trash-alt text-danger"></i>&emsp;Delete</a>
                </div>
                </div>
                </div>
                ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('store.contactPage');
    }

    public function contactDetail($id){
        DB::table('contact')->where('id',$id)->update([
            'status' => 'Y',
        ]);
        $data = DB::table('contact')->where('id',$id)->get();

        return view('store.contactDetail',compact('data'));
    }

    public function sendEmail(Request $request){
        $name = Auth::user()->name;
        $validator = Validator::make(request()->all(), [
            'user_name' => 'required',
            'user_email'     => 'required',
            'message'=> 'required',
            'your_email'    => 'required',

        ]);

        if ($validator->fails()) {
            $result = validationErrorsToString($validator->errors());
            return response()
            ->json(['status' => false, 'description' =>  $result]); 
        }else{
                Mail::send('store.email', ['name' => $request->user_name,'name_admin' => $name, 'description' => $request->message,'email' => $request->user_email ], function ($message) use ($request,$name)
                {
                    $message->subject('CekSembakoAjaa Contact');
                    $message->from($request->your_email,$name);
                    $message->to($request->user_email);
                });
                return response()
                ->json(['status' => true, 'description' =>  str_replace("#", 'Message', SENDING_SUCCESSFULLY_MESSAGE)]);  
            
            

            
        }
    }

    public function messageDelete($id)
    {
        DB::table('contact')->where('id', $id)
        ->delete();
        return response()
        ->json(['status' => true, 'description' =>  str_replace("#", 'Message',DELETE_SUCCESSFULLY_MESSAGE)]);

    }

    
}

