@extends('template.master')

@section('title')
Store Profile
@endsection

@section('css')

@endsection

@section('content')
@foreach($data as $item)
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Store Profile</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Store</a></li>
						<li class="breadcrumb-item active">Store Profile</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">

					<!-- Profile Image -->
					<div class="card card-primary card-outline">
						<div class="card-body box-profile">
							<div class="text-center">
								<img class="rounded-circle"
								src="{{ asset('public/storeLogo/'.$item->image) }}"
								alt="store profile picture" width="150" height="150">
							</div>

							<h3 class="profile-username text-center">{{ $item->name }}</h3>

							<p class="text-muted text-center">Store</p>

						</div>
						<!-- /.card-body -->
					</div>
				</div>
				<!-- /.card -->
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-6">
									<h5 style="margin-top: 10px"><b>Detail Store</b></h5>
								</div>
								<div class="col-6">
									<a class="btn btn-icons btn-danger float-right" href="/storeindex"> <i class="fa fa-arrow-left" style="color: white;"></i> </a>
								</div>
							</div>
							
						</div><!-- /.card-header -->
						<div class="card-body">
							<div class="tab-content">
								<div class="active tab-pane" id="activity">
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Store Name</b> </div>
										<div class="col-6"> {{ $item->name }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Email</b> </div>
										<div class="col-6"> {{ $item->email }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Address </b> </div>
										<div class="col-6"> {{ $item->address }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Phone Number</b> </div>
										<div class="col-6"> {{ $item->phone }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Description</b> </div>
										@if($item->description == null)
										<div class="col-6 text-danger">Belum di input</div>
										@else
										<div class="col-6"> {{ $item->description }}</div>
										@endif
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Open Time</b> </div>
										<div class="col-6"> {{ $item->open_time }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Closed Time</b> </div>
										<div class="col-6"> {{ $item->closed_time }}</div>
									</div>


								</div>
								
							</div>
							<!-- /.tab-content -->
						</div><!-- /.card-body -->
					</div>
					<!-- /.nav-tabs-custom -->
				</div>
			</div>
		</div>
	</section>
	@endforeach
	@endsection