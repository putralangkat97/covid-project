@extends('template.master')

@section('title')
Store Log
@endsection

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Store Log</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Store Log</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-5">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
				<p class="text-muted text-center">Your Login</p>
                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>IP Address</b> <a class="float-right">{{ $ip }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Login Time</b> <a class="float-right">{{ Auth::user()->updated_at }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Browser</b> <a class="float-right">{{ $browser }}</a>
                  </li>
					<li class="list-group-item">
                    <b>OS</b> <a class="float-right">{{ $os }}</a>
                  </li>
                  </li>
					<li class="list-group-item">
                    <b>Country Name</b> <a class="float-right">{{ $location->countryName }}</a>
                  </li>
                </ul>

              </div>
            </div>
              <!-- /.card-body -->
            </div>
            <div class="col-5">
              <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Store Online</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php if ($data = App\Model\StoreAdmin::where('is_active','=','Y')->get()) :
                   ?>
                   <?php foreach ($data as $item) : ?>
                


                <strong><i class="far fa-dot-circle"></i> {{ $item->name }}</strong>

                <p class="text-muted">{{ $item->email }}</p>
          
                <hr>
              <?php endforeach; ?>
                <?php endif; ?>
                <?php $data = App\Model\StoreAdmin::where('is_active','=','Y')->get();  if(count($data) == 0) : ?>
                <strong>Tidak ada yang online</strong>
              <?php endif; ?>
              </div>
              <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
</section>
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>

@endsection