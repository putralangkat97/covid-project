@extends('template.master')

@section('title')
Store
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">





@endsection


@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Store</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Store</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <div class="card">
    <div class="card-header">
      <a class="btn btn-icons btn-success float-right" href="javascript:void(0)" id="createNewStore"> <i class="fa fa-plus" style="color: white;"></i> </a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="user_table" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Store Image</th>
            <th>Store Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>

  <!-- Modal -->
  <div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="formStaffLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modelHeading"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" id="listform" method="POST" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" id="id">  
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Store Name</label>
              <div class="col-lg-9">
                <input type="text" name="store_name" id="store_name" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Email</label>
              <div class="col-lg-9">
                <input type="email" name="Email" id="email" class="form-control" required="">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Password</label>
              <div class="col-lg-9">
                <input type="password" name="Password" id="password" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Address</label>
              <div class="col-lg-9">
                <textarea rows="3" cols="3" id="address" name="Address" class="form-control"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Phone Number</label>
              <div class="col-lg-9">
                <input type="number" id="telp" name="Phone_Number" class="form-control">
              </div>
            </div>
            <div class="form-group row">
            <label class="col-form-label col-lg-3" style="font-size: 15px;">Store Image</label>
            <div class="col-lg-9">
              <input type="file" accept="image/*" name="image" id="image-preview">
              <input type="text" class="form-control" name="gambar" id="gambar-id" hidden>
            </div>
          </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Open Time</label>
              <div class="col-lg-9">
                <div class="row">
                  <div class="col-5">
                    <input class="form-control" type="time" id="open_time" name="open_time">
                  </div>
                  <label for="" class="col-form-label" style="font-size: 15px;">to</label>
                  <div class="col-5">
                    <input class="form-control" type="time" id="closed_time" name="closed_time">
                  </div>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success" id="saveBtn">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Detail Store -->
  <div class="modal fade" id="detailStoreModal" tabindex="-1" role="dialog" aria-labelledby="formStaffLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalHead"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" enctype="multipart/form-data">
            <div class="row">
              <h5 id="store_name"></h1>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Delete Sweet Alert -->
  <div class="modal fade" role="dialog"  tabindex="-1" id="confirmationModal" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <img src="/public/images/sweetalert.png" alt="alert" style="margin-left:39%; margin-bottom: 0px; margin-top: 0px">
          <h3 class="text-center" style="margin-top:20px;">Are you Sure ?</h3>
          <p class="text-center" style="margin-top:10px; margin-bottom:0px;">Data cannot be returned</p>
        </div>
        <div class="modal-footer" style="margin-right:30%; margin-top:-10px;">
          <button type="button" name="ok_button" id="ok_button" class="btn btn-success">Yes, Delete</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div> 
  {{-- Scripts --}}
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>

  <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('js/sweetalert.min.js') }}"></script>
  <script>
    $(document).ready( function () {
     $.ajaxSetup({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
     $('#user_table').DataTable({
      destroy:true,
      processing: true,
      serverSide: false,
      paging: true,
      lengthChange: true,
      searching: true,
      ordering: true,
      info: false,
      autoWidth: false,
      responsive: true,
      ajax :{
        url: "{{ route('store.index') }}",
        type: "GET",
      },
      columns: [
      {data: 'image', name: 'image', render: function(data, type, full, meta){ 
        return "<center><img src=public/storeLogo/" + data + " width='50' height='50' class='rounded-circle' alt=''></img></center>";
      }},
      { data: 'name', name: 'name' },
      { data: 'email', name: 'email' },
      { data: 'phone', name: 'phone' },
      { data: 'is_active', name: 'is_active',render:function(data){
       if (data=="Y") {
        return '<span class="badge badge-success">ACTIVE</span>';
      }else if(data=="N"){
        return '<span class="badge badge-danger">NOT ACTIVE</span>';
      }
    }},

    {data: 'action', name: 'action', orderable: false,searchable: false},
    ],
  });
     $('#createNewStore').click(function () {
      $('#saveBtn').val("create-product");
      $('#id').val('');
      $('#listform').trigger("reset");
      $('#modelHeading').html("<b>Add new Store</b>");
      $('#ajaxModal').modal('show');
    });


     $('#saveBtn').click(function (e) { 
      e.preventDefault(); 
      $(this).html('Saving...');  
      $.ajax({ 
        data: $('#listform').serialize(), 
        url: "{{ route('store.add') }}", 
        type: "POST", 
        dataType: 'json', 
        success: function (data) {  
          if (data.status == false) {
            swal("Oops!", data.description, "error");
          }else {
            swal("Success!", data.description, "success");
            $('#listform').trigger("reset"); 
            $('#modal').modal('hide'); 
            $('#saveBtn').html('Add'); 
            window.location.reload(); 


          } 

        } 
      });

    }); 
     $('#image-preview').change(function () {
      if ($(this).val() != '') { 
        upload(this);
      }else{ 
      }
    });

     function upload(img) {
      var form = $('#listform')[0];
      var form_data = new FormData(form);
      form_data.append('image', img.files[0]);
      form_data.append('_token', '{{csrf_token()}}');
      $.ajax({
        url: "/store/uploadImage",
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function (data) {
          if (data.fail) { 
            alert(data.errors['image']);
          }
          else {
            console.log('gambar', data)
            $('#gambar-id').val(data); 
          }        },
          error: function (xhr, status, error) {
            alert(xhr.responseText); 
          }
        });
    }

    // Update Data
    $('body').on('click', '.updateStore', function () { 
      var staff_id = $(this).data('id'); 
      $.get("store/update/"+staff_id, function (data) { 
        $('#modelHeading').html("<b>Update Store</b>"); 
        $('#saveBtn').val("edit-user"); 
        $('#ajaxModal').modal('show'); 
        $('#id').val(data[0]['id']); 
        $('#store_name').val(data[0]['name']); 
        $('#address').val(data[0]['address']); 
        $('#telp').val(data[0]['phone']); 
        $('#password').val(data[0]['password']);  
        $('#email').val(data[0]['email']); 
        $('#open_time').val(data[0]['open_time']); 
        $('#closed_time').val(data[0]['closed_time']);  
        $('#gambar-id').val(data[0]['image']);   


      }) 
    });


    var id;
    $(document).on('click','.deleteStore',function(){
      id = $(this).data("id"); 
      console.log('id',id);
      $('#confirmationModal').modal('show');
    });

    $('#ok_button').click(function(){
      console.log('id',id);
      $.ajax({ 
        type: "DELETE", 
        url: "/store/delete/"+id, 
        data:{
          "_token":"{{ csrf_token() }}",
        },
        beforeSend:function(){
          $('#ok_button').text('Deleting...');
        },
        success: function (data) { 
          $('#confirmationModal').modal('hide');
          swal("Success", data.description, "success"); 
          $('#ok_button').text('Yes, Delete');

          location.reload(); 
        }, 
        error: function (data) { 
          console.log('Error:', data); 
        }  
      });  
    });

  });
</script>
@endsection