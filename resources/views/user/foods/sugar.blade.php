@extends('user.templates.master')


@section('title')
    Sugar - Detail Food
@endsection

@section('content')
    <!-- bread-crumb start here -->
<div class="bread-crumb">
    <img src="{{ asset('dist/images/top-banner.jpg') }}" class="img-responsive" alt="banner-top" title="banner-top">
	<div class="container">
		<div class="matter">
			<h2><span>DETAIL FOOD</span></h2>
			<ul class="list-inline">
				<li>
					<a href="{{ route('user.index') }}">HOME</a>
				</li>
				<li>
					<a href="{{ route('detail.sugar') }}">DETAIL FOOD</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- bread-crumb end here -->

<!-- shop detail start here -->
<div class="shopdetail">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
				<div class="row">
					<!--thumb image code start-->
					<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
						<a class="thumbnail"><img src="{{ asset('public/foodLogo/gula.png') }}" title="img" alt="img" /></a>
					</div>
					<!--thumb image code end-->

					<!--Product detail code start-->
					<div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
						<h5> <span>Sugar</span></h5>
						<p class="shortdes">
                            <b>Sugar</b> is the common name for a number of chemical substances, some of which have a sweet taste. Mostly, it refers to either sucrose, lactose, or fructose. Sugar is contained in certain kinds of food, or it is added to give a sweet taste. Sugar is extracted from certain plants, such as sugarcane or sugar beet.
						</p>
						<hr>
						<h5>Product Price</h5>
						<div class="price">
              Start From <?php $data = App\Model\BasicNeed::where('type','=','Sugar')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
    					Rp.@convert($item->minPrice)
              @endforeach
						</div>
					</div>
				</div>
				<!--Product detail code end-->
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#tab-description" data-toggle="tab">Store Information</a>
					</li>
				</ul>
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
						<div class="tab-content">
							<b>List of stores that have sugar </b>
							<div class="tab-pane active" id="tab-description" style="margin-top:15px">
                <table id="datatable" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Merk</th>
                      <th>Store Name</th>
                      <th>Value</th>
                      <th>Price</th>
                      <th>Description</th>
					  <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
							</div>
						
						</div>
					</div>
				</div>
				<br>
				<br>
			</div>
		</div>
	</div>
</div>
<!-- shop end here -->
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.colVis.min.js"></script>
<script >
     $(document).ready( function () {
     $.ajaxSetup({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
  $('#datatable').DataTable({
     destroy:true,
      processing: true,
      serverSide: true,
      paging: true,
      lengthChange: true,
      searching: true,
      ordering: true,
      info: false,
      autoWidth: false,
      responsive: true,
    ajax : "{{ route('detail.sugar') }}",
    columns: [
      {data: 'item', name: 'basic_needs.item'},
      {data: 'name', name: 'admin.name'},
      {data: 'value', name: 'basic_needs.value'},
      {data: 'price', name: 'basic_needs.price',render: $.fn.dataTable.render.number( '.', ',', 2, 'Rp.' )},
      {data: 'description' , name:'basic_needs.description'},
      {data: 'action', name: 'action', orderable: false, searchable: false},

    ]
  });

});
</script>
@endsection
