@extends('user.templates.master')


@section('title')
    Detail Store
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">

@endsection

@section('content')
@foreach ($data as $item)
    <!-- bread-crumb start here -->
<div class="bread-crumb">
    <img src="{{ asset('dist/images/top-banner.jpg') }}" class="img-responsive" alt="banner-top" title="banner-top">
	<div class="container">
		<div class="matter">
			<h2><span>DETAIL STORE</span></h2>
			<ul class="list-inline">
				<li>
					<a href="{{ route('user.index') }}">HOME</a>
				</li>
				<li>
					<a href="">DETAIL STORE</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- bread-crumb end here -->

<!-- shop detail start here -->
<div class="shopdetail">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
				<div class="row">
					<!--thumb image code start-->
					<div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
						<a class="thumbnail"><img src="{{ asset('public/storeLogo/'.$item->image) }}" title="img" alt="img" /></a>
					</div>
					<!--thumb image code end-->

					<!--Product detail code start-->
					<div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
						<h5> <span>{{ $item->name }}</span></h5>
						<p class="shortdes">
                          {{ $item->description }}
						</p>
						<hr>
						<h5>Detail Of Store</h5>
						<ul class="list-unstyled featured">
							<li><span class="glyphicon glyphicon-envelope"></span>&emsp;{{ $item->email }}</li>
                            <li><span class="glyphicon glyphicon-home"></span>&emsp;{{ $item->address }}</li>
                            <li><span class="glyphicon glyphicon-earphone"></span>&emsp;{{ $item->phone }}</li>
                        </ul>
                        <hr>
                        <div class="price">
                            Open From <?= (new DateTime($item->open_time))->format("H:i"); ?> to  <?= (new DateTime($item->closed_time))->format("H:i"); ?>
                        <hr>
                        </div>
					</div>
				</div>
				<!--Product detail code end-->
				<ul class="nav nav-tabs">
					<li class="active">
            <a href="#tab-description" data-toggle="tab">List of BasicNeed</a>
         </li>
          <li>
            <a href="#tab-add" data-toggle="tab">Contact Store</a>
					</li>
				</ul>
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
						<div class="tab-content">
              <div class="tab-pane active" id="tab-description">
                <table id="datatable" class="table table-bordered"  style="margin-top:15px">
                  <thead>
                    <tr>
                      <th>Merk</th>
                      <th>Type</th>
                      <th>Value</th>
                      <th>Price</th>
                      <th>Description</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>

              <div class="tab-pane" id="tab-add">
								<form class="form-horizontal" id="form-review" >
									@csrf
									<input type="hidden" id="id" name="id" value="{{ $item->id }}">
									<h2>Contact Store</h2>
									
									<div class="form-group required">
										<div class="col-md-6 col-sm-12">
											<label class="control-label" for="input-name">Name</label>
											<input type="text" name="name" value="" placeholder="Enter your Name" required="" id="input-name" class="form-control" />
										</div>
										<div class="col-md-6 col-sm-12">
											<label class="control-label" for="input-email">Email</label>
											<input type="email" name="email" value="" required="" id="input-email" placeholder="Enter your Email" class="form-control" />
										</div>
									</div>

									<div class="form-group required">
									<div class="col-sm-12">
											<label class="control-label" for="input-phone">Phone Number</label>
											<input type="number" name="phone" value="" placeholder="Enter your Phone Number" required="" id="input-name" class="form-control" />
										</div>
									</div>
									<div class="form-group required">
										<div class="col-sm-12">
											<label class="control-label" for="input-review">Message</label>
											<textarea rows="4" id="input-review"  required="" placeholder="Enter your Message" class="form-control" name="description"></textarea>
										</div>
									</div>
									<div class="buttons clearfix">
									  <div class="pull-right">
										<button type="submit" id="saveBtn"  class="btn btn-primary">Send</button>
									  </div>
									</div>
								</form>
							</div>


              
              
						
						</div>
					</div>
				</div>
				<br>
				<br>
			</div>
		</div>
	</div>
</div>
@endforeach

<!-- shop end here -->
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
  <script src="{{ asset('js/sweetalert.min.js') }}"></script>

<script >
     $(document).ready( function () {
     $.ajaxSetup({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });


  $('#datatable').DataTable({
     destroy:true,
      processing: true,
      serverSide: true,
      paging: true,
      lengthChange: true,
      searching: true,
      ordering: true,
      info: false,
      autoWidth: false,
      responsive: true,
    ajax : {
        type: "GET",
        url:"{{ route('show.data',request()->segment(3)) }}"
    },
    columns: [
      {data: 'item', name: 'item'},
      {data: 'type', name: 'type'},
      {data: 'value', name: 'value'},
      {data: 'price', name: 'price',render: $.fn.dataTable.render.number( '.', ',', 2, 'Rp.' )},
      {data: 'description' , name:'description'},

    ]
  });

  $('#saveBtn').click(function (e) { 
      e.preventDefault(); 
      $(this).html('Sending...');  
      $.ajax({ 
        data: $('#form-review').serialize(), 
        url: "{{ route('message.send') }}", 
        type: "POST", 
        dataType: 'json', 
        success: function (data) {  
          if (data.status == false) {
            swal("Oops!", data.description, "error");
          }else {
            swal("Success!", data.description, "success");
            $('#form-review').trigger("reset"); 
            $('#saveBtn').html('Sending'); 
            window.location.reload(); 


          } 

        } 
      });

    }); 

});
</script>

@endsection
