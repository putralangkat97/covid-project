@section('title')
    About Us
@endsection

<!DOCTYPE html>
<html lang="en">
    @include('user.layouts.head')
<!-- Body -->
<body class="header3">
<!--top start here -->
@include('user.partials._1top')
    <header>
    @include('user.partials._2header')
    </header>

    <!-- bread-crumb start here -->
    <div class="bread-crumb">
        <img src="{{ asset('dist/images/top-banner.jpg') }}" class="img-responsive" alt="banner-top" title="banner-top">
        <div class="container">
            <div class="matter">
                <h2><span> About Us</span></h2>
                <ul class="list-inline">
                    <li>
                        <a href="{{ route('user.index') }}">HOME</a>
                    </li>
                    <li>
                        <a href="{{ route('about.user') }}">About us</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- bread-crumb end here -->

    <!-- organic start here -->
    <div class="organic">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 commontop text-center">
                    <h4>
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i> 
                        Welcome to CekSembakoAjaa.
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i>
                    </h4>
                    <p>Pellentesque sed posuere nisi. Nunc nec looreet mauris. Etiam valutpat ligula eu lacus varius scelerisque. Morbi fringilla euismod semper.</p>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <p class="des">Pellentesque sed posuere nisi. Nunc nec looreet mauris. Etiam valutpat ligula eu lacus varius scelerisque. Morbi fringilla euismod semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue quis quam eget efficitur. Vestibulum venenatis tortor ipsum, at venenatis tellus aliquet eu. Orci varius natoque penatibus. Pellentesque sed posuere nisi. Nunc nec looreet mauris.</p>
                    <p class="des2"><i class="icon_quotations first"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel venenatis eros. Integer ultricies dapibus nulla, ac malesuada erat pulvinar eget. <i class="icon_quotations last"></i></p>
                    <p class="des">Pellentesque sed posuere nisi. Nunc nec looreet mauris. Etiam valutpat ligula eu lacus varius scelerisque. Morbi fringilla euismod semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue quis quam eget efficitur. Vestibulum venenatis tortor ipsum, at venenatis tellus aliquet eu. Orci varius natoque penatibus. Pellentesque sed posuere nisi. Nunc nec looreet mauris.</p>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                    <img src="{{ asset('dist/images/about/lemon-banner.png') }}" class="img-responsive" alt="banner" title="banner" />
                </div>
            </div>
        </div>
    </div>
    <!-- organic end here -->

    <!-- team start here -->
    <div class="team back">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 commontop text-center">
                    <h4>
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i> 
                        our team members
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i>
                        <i class="icon_star_alt"></i>
                    </h4>
                    <p>This is our Team Members</p>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                    <div class="box">
                        <img src="{{ asset('public/avatar/yoel.png') }}" width="270" height="270" class="img-responsive" alt="img" title="img">
                        <h3>Yoel Nainggolan</h3>
                        <p>Lead Developer</p>
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/" target="_blank">
                                    <i class="social_facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/" target="_blank">
                                    <i class="social_twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/" target="_blank">
                                    <i class="social_googleplus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/" target="_blank">
                                    <i class="social_instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.linkedin.com/" target="_blank">
                                    <i class="social_linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                    <div class="box">
                        <img src="{{ asset('public/avatar/bang anggit.jpg') }}" width="270" height="270" class="img-responsive" alt="img" title="img">
                        <h3>Anggit Ari Utomo</h3>
                        <p>Developer</p>
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/" target="_blank">
                                    <i class="social_facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/" target="_blank">
                                    <i class="social_twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/" target="_blank">
                                    <i class="social_googleplus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/" target="_blank">
                                    <i class="social_instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.linkedin.com/" target="_blank">
                                    <i class="social_linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- team end here -->

    @include('user.partials._6footer')

    @include('user.partials._7script')
</body>
</html>

