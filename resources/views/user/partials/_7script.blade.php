<script src="{{ asset('/dist/js/jquery.2.1.1.min.js') }}"></script>
<!-- bootstrap js -->
<script src="{{ asset('/dist/bootstrap/js/bootstrap.min.js') }}"></script>
<!--bootstrap select-->
<script src="{{ asset('/dist/js/dist/js/bootstrap-select.js') }}"></script>
<!--internal js-->
<script src="{{ asset('/dist/js/internal.js') }}"></script>
<!-- owlcarousel js -->
<script src="{{ asset('/dist/js/owl-carousel/owl.carousel.min.js') }}"></script>