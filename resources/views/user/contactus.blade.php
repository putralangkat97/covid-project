@extends('user.templates.master')

@section('title')
Contact Us
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('content')
<!-- bread-crumb start here -->
<div class="bread-crumb">
	<img src="{{ asset('dist/images/top-banner.jpg') }}" class="img-responsive" alt="banner-top" title="banner-top">
	<div class="container">
		<div class="matter">
			<h2><span>Contact us</span></h2>
			<ul class="list-inline">
				<li>
					<a href="{{ route('user.index') }} ">HOME</a>
				</li>
				<li>
					<a href="{{ route('contact.user') }}">Contact us</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- bread-crumb end here -->

<!-- contactus start here -->
<div class="contactus">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 commontop text-center">
				<h4>
					<i class="icon_star_alt"></i>
					<i class="icon_star_alt"></i>
					<i class="icon_star_alt"></i> 
					Contact us
					<i class="icon_star_alt"></i>
					<i class="icon_star_alt"></i>
					<i class="icon_star_alt"></i>
				</h4>
				<p>Message the Super Admin if there are Bugs or suggestion.</p>
			</div>
			<div class="col-sm-offset-2 col-md-8 col-sm-8  col-xs-12">
				<form method="" id="form-review" enctype="multipart/form-data" class="form-horizontal">
					@csrf
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="name" value="" required="" id="input-name" class="form-control" placeholder="Name *"/>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="email" name="email" value="" required="" id="input-email" class="form-control" placeholder="Email *"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<input type="number" name="phone" required="" value="" id="input-subject" class="form-control" placeholder="Phone Number *"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 col-md-12 col-xs-12">
							<i class="icofont icofont-pencil-alt-5"></i>
							<textarea name="message" required="" id="input-enquiry" class="form-control" placeholder="Your Message *"></textarea>
						</div>
					</div>
					<div class="buttons text-right">
						<button class="btn btn-primary" type="submit" id="saveBtn">SEND MESSAGE </button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- contactus end here -->

<!-- address start here -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="address">
				<ul class="list-inline">
					<li>
						<i class="icon_map_alt"></i>
						Medan, Sumatera Utara <br>Indonesia. 
					</li>
					<li>
						<i class="fa fa-envelope-o"></i>
						ceksembako@gmail.com
					</li>
					<li>
						<i class="icon_mobile"></i>
						+62 819-1886-1155
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- address end here -->
<!-- Scripts -->
 <script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

<script>
	 $('#saveBtn').click(function (e) { 
      e.preventDefault(); 
      $(this).html('Sending...');  
      $.ajax({ 
        data: $('#form-review').serialize(), 
        url: "{{ route('user.send') }}", 
        type: "POST", 
        dataType: 'json', 
        success: function (data) {  
          if (data.status == false) {
            swal("Oops!", data.description, "error");
          }else {
            swal("Success!", data.description, "success");
            $('#form-review').trigger("reset"); 
            $('#saveBtn').html('Sending'); 
            window.location.reload(); 


          } 

        } 
      });

    }); 
</script>

@endsection