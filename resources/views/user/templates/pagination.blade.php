@include('user.layouts.head')
@if ($paginator->hasPages())
    <nav>
        <ul class="list-inline pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
            <li class="pull-right">
              <a class="disabled" aria-label="true" rel="prev">Previous</a>
            </li>
                <!-- <li class="disabled" aria-disabled="true"><span>@lang('pagination.previous')</span></li> -->
            @else
            <li class="pull-right">
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev">Previous</a>
            </li>

            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
            <li class="pull-right">
              <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a></li>
            </li>
            @else
            <li class="pull-right">
                <a class="disabled" aria-disabled="true" rel="next"><span>Next</span></a>
              </li>
            @endif
        </ul>
    </nav>
@endif
