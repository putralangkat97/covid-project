<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Bootstrap stylesheet -->
    <link href="{{ asset('/dist/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <!-- crousel css -->
    <link href="{{ asset('/dist/js/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <!--bootstrap select-->
    <link href="{{ asset('/dist/js/dist/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
    <!-- font -->
    <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@200;400;500;600&display=swap" rel="stylesheet">
    <!-- font-awesome -->
    <link href="{{ asset('/dist/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dist/css/ele-style.css') }}" rel="stylesheet" type="text/css"/>
    <!-- stylesheet -->
    <link rel="stylesheet" href="{{ asset('dist/js/preetycheble/prettyCheckable.css') }}"/>

    <link href="{{ asset('/dist/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Databtable -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    @yield('css')

<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
</head>
<body>
<!--top start here -->
@include('user.templates.header')
@yield('content')
@include('user.templates.footer')


</body>
</html>
