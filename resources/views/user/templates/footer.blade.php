<!-- footer start here -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                <img src="{{ asset('public/images/logo_footer.png') }}" class="img-responsive" title="logo" alt="logo">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam congue quis quam eget efficitur. Vestibulum venenatis tortor ipsum, at venenatis tellus aliquet eu. Orci varius natoque penatibus...</p>
                <ul class="list-unstyled contact">
                    <li>
                        <a href="contactus.html">
                            <i class="fa fa-map-marker"></i> Medan, Sumatera Utara, Indonesia.
                        </a>
                    </li>
                    <li>
                        <a href="contactus.html">
                            <i class="icon_phone"></i> Phone - +62 819-1886-1155
                        </a>
                    </li>
                    <li>
                        <a href="contactus.html">
                            <i class="fa fa-envelope"></i> Email - ceksembako@gmail.com
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-9 col-sm-9 col-lg-9 col-xs-12">
                <div id="newsletter">
                    <form class="form-horizontal" name="subscribe">
                        <div class="form-group">
                            <div class="input-group">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                        <h5>Categories</h5>
                        <hr>
                        <ul class="list-unstyled">
							<li>
								<a href="{{ route('detail.rice') }}">Rice</a>
							</li>
							<li>
								<a href="{{ route('detail.milk') }}">Milk</a>
							</li>
							<li>
								<a href="{{ route('detail.meat') }}">Meats</a>
							</li>
							<li>
								<a href="{{ route('detail.oil') }}">Cooking Oil</a>
							</li>
							<li>
								<a href="{{ route('detail.salt') }}">Salt</a>
							</li>
						</ul>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                        <h5>Stay Connected</h5>
                        <hr>
                        <ul class="list-unstyled social">
                            <li>
                                <a href="https://www.facebook.com/" target="_blank">
                                    <i class="social_facebook"></i> Facebook
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/" target="_blank">
                                    <i class="social_twitter"></i> Twitter
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/" target="_blank">
                                    <i class="social_googleplus"></i> Google+
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/" target="_blank">
                                    <i class="social_youtube"></i> Youtube
                                </a>
                            </li>
                            <li>
                                <a href="https://in.linkedin.com/" target="_blank">
                                    <i class="social_linkedin"></i> Linked In
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="powered">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                    <p>© Copyright 2020, <span>Cek Sembako Ajaa.</span>  All Rights Reserved.</p>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 text-right">
                    <ul class="list-inline">
						<li>
							<img src="{{ asset('/dist/images/card1.png') }}" class="img-responsive" alt="card" title="card" />
						</li>
						<li>
							<img src="{{ asset('/dist/images/card2.png') }}" class="img-responsive" alt="card" title="card" />
						</li>
						<li>
							<img src="{{ asset('/dist/images/card3.png') }}" class="img-responsive" alt="card" title="card" />
						</li>
						<li>
							<img src="{{ asset('/dist/images/card4.png') }}" class="img-responsive" alt="card" title="card" />
						</li>
						<li>
							<img src="{{ asset('/dist/images/card5.png') }}" class="img-responsive" alt="card" title="card" />
						</li>
						<li>
							<img src="{{ asset('/dist/images/card6.png') }}" class="img-responsive" alt="card" title="card" />
						</li>
					</ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer end here -->

<!-- <script src="{{ asset('/dist/js/jquery.2.1.1.min.js') }}"></script> -->
<!-- bootstrap js -->
<script src="{{ asset('/dist/bootstrap/js/bootstrap.min.js') }}"></script>
<!--bootstrap select-->
<script src="{{ asset('/dist/js/dist/js/bootstrap-select.js') }}"></script>
<!--internal js-->
<script src="{{ asset('/dist/js/internal.js') }}"></script>
<script src="{{ asset('/dist/js/preetycheble/prettyCheckable.min.js') }}"></script>
<script src="{{ asset('/dist/js/check.js') }}"></script>
<script src="{{ asset('/dist/js/internal.js') }}"></script>



<!-- owlcarousel js -->
<script src="{{ asset('/dist/js/owl-carousel/owl.carousel.min.js') }}"></script>
