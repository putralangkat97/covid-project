<!-- header start here-->
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                <div id="logo">
                    <a href="{{ route('user.index') }}">
                        <img class="img-responsive" src="{{ asset('public/images/logo.png') }}" alt="logo" title="logo" />
                    </a>
                </div>
            </div>  
            <div class="col-md-11 col-sm-11 col-lg-11 col-xs-12">
                <!-- menu start here -->
                <div id="menu"> 
                    <nav class="navbar">
                        <div class="navbar-header">
                            <span class="menutext visible-xs">Menu</span>
                            <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="btn btn-navbar navbar-toggle" type="button"><i class="fa fa-bars"></i></button>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex1-collapse padd0">
                            <ul class="nav navbar-nav text-right">
                                <li>
                                    <a href=""></a>
                                </li>   
                                <li>
                                    <a href=""></a>
                                </li>
                                
                                <li class="dropdown topheader">
                                    <a href="{{ route('user.index') }}">Home</a>
                                </li>
                               
                                <li>
                                    <a href="{{ route('about.user') }}">About us</a>
                                </li>
                                <li>
                                    <a href=""></a>
                                </li>
                                <li class="dropdown topheading">
                                    <a href="{{ route('store.user') }}">Store</a>
                                </li>
                                <li>
                                    <a href="{{ route('contact.user') }}">Contact us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <!-- menu end here -->
            </div>
        </div>
    </div>
</header>
<!-- header end here -->