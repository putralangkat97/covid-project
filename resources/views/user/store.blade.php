@extends('user.templates.master')

@section('title')
Store
@endsection

@section('content')
<!-- bread-crumb start here -->
<div class="bread-crumb">
    <img src="{{ asset('dist/images/top-banner.jpg') }}" class="img-responsive" alt="banner-top" title="banner-top">
    <div class="container">
        <div class="matter">
            <h2><span>STORE</span></h2>
            <ul class="list-inline">
                <li>
                    <a href="index-2.html">HOME</a>
                </li>
                <li>
                    <a href="shop.html">Store</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- bread-crumb end here -->

<!-- shop container start here -->
<div class="shop">
    <div class="container">
        <div class="row">
            <div class="sort col-sm-12 col-md-12 col-lg-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 list hidden-xs text-right">
						<div class="btn-group btn-group-sm">
							<button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid"><i class="icon_grid-2x2"></i></button>
							<button  type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-list-ul" ></i></button>
						</div>
					</div>
                </div>
                <p>Show result <?= count($data); ?></p>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12 hidden-xs">
                <div class="leftside">

                    <div class="highlight">
                        <h3>Category</h3>
                        <ul class="list-unstyled">
                            <li>
                                
                                <a href="{{ route('detail.rice') }}">Rice</a>
                            </li>
                            <li>
                               
                                <a href="{{ route('detail.sugar') }}">Sugar</a>
                            </li>
                            <li>
                               
                                <a href="{{ route('detail.meat') }}">Meat</a>
                            </li>
                            <li>
                               
                                <a href="{{ route('detail.salt') }}">Salt</a>
                            </li>
                            <li>
                               
                                <a href="{{ route('detail.oil') }}">Cooking Oil</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-sm-9">
                <div class="row">
                    <?php foreach ($data as $item): ?>

                    <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="product-thumb">
                            <div class="image">
                                <a class="rec-fill front front--slide-left" href="detail/store/{{ $item->id }}"><img src="{{ asset('public/storeLogo/'.$item->image) }}" style="display: block; margin-left: auto;
                                    margin-right: auto; alt="image" title="image" width="270" height="264"  /></a>
                                <a class="rec-fill back back--slide-left" href="detail/store/{{ $item->id }}"><img src="{{ asset('public/storeLogo/'.$item->image) }}" style="display: block; margin-left: auto;
                                    margin-right: auto; alt="image" title="image" width="270" height="264" /></a>
                            </div>
                            <div class="caption">
                                <h4><a href="shopdetail.html">{{ $item->name }}</a></h4>
                                <p class="price">{{ $item->email }}</p>
								<p class="des">{{ $item->address }}</p>
								<div class="button-group">
									<button type="button"><i class="fa fa-shopping-cart"></i></button>
									<button type="button"><i class="fa fa-heart"></i></button>
									<button type="button"><i class="fa fa-retweet"></i></button>
								</div>
                            </div>
                        </div>
                    </div>
            <?php endforeach ?>


            </div>
            {{ $data->links() }}

            <!--pagination code end here-->
        </div>
        <!--pagination code start here-->

    </div>
</div>
</div>
<!-- shop container end here -->
<script src="{{ asset('/dist/js/jquery.2.1.1.min.js') }}"></script>

@endsection
