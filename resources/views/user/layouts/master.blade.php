
<!DOCTYPE html>
<html lang="en">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<!-- Head -->
@include('user.layouts.head')

<!-- Body -->
<body class="header3">
<!--top start here -->
@include('user.partials._1top')

<!-- header start here-->
<header>
@include('user.partials._2header')
</header>

<!-- slider start here -->
@include('user.partials._3slider')

<!-- about start here -->
@include('user.partials._4about')

<!-- product start here -->
<div class="product">
	@yield('product')
</div>

<!-- banner start here -->
@include('user.partials._5banner')

<!-- bestdeal start here -->
<div class="container">
	@yield('bestdeal')
</div>

<!-- mixveg start here -->
<div class="mixveg">
	@yield('mixveg')
</div>

<!-- footer start here -->
@include('user.partials._6footer')
<!-- footer end here -->

<!-- JAVASCRIPT -->
<!-- jquery -->
@include('user.partials._7script')
@yield('script')
</body>

</html>