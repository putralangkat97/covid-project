@extends('user.layouts.master')

@section('title')
	Home
@endsection

@section('product')
<div class="container">
	<div class="row">
		<div class="commontop text-center">
			<h4>Basic Need's Lists</h4>
		</div>
		<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
			<div class="product-thumb">
				<div class="image">
						<img src="{{ asset('public/foodLogo/rice.png') }}" alt="image" title="image" class="img-responsive" />
					<div class="onhover1">
						<div class="button-group">

							<a href="{{ route('detail.rice') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>

						</div>
					</div>
				</div>
				<div class="caption text-center">
					<h4><a href="#">Rice</a></h4>
					<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Rice')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
					Rp.@convert($item->minPrice)
				<?php endforeach; ?>
			</p>
		</div>
	</div>	
</div>
<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
	<div class="product-thumb">
		<div class="image">
				<img src="{{ asset('public/foodLogo/cooking_oil.png') }}" alt="image" title="image" class="img-responsive" />
			<div class="onhover1">
				<div class="button-group">
					<a href="{{ route('detail.oil') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>
				</div>
			</div>
		</div>
		<div class="caption text-center">
			<h4><a href="#">Cooking Oil</a></h4>
			<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Cooking Oil')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
			Rp.@convert($item->minPrice)
		<?php endforeach; ?>
	</p>
</div>
</div>	
</div>
<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
	<div class="product-thumb">
		<div class="image">
				<img src="{{ asset('public/foodLogo/sugar.png') }}" alt="image" title="image" class="img-responsive" />
			<div class="onhover1">
				<div class="button-group">
					<a href="{{ route('detail.sugar') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>
				</div>
			</div>
		</div>
		<div class="caption text-center">
			<h4><a href="#">Sugar</a></h4>
			<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Sugar')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
			Rp.@convert($item->minPrice)
		<?php endforeach; ?>
	</p>
</div>
</div>	
</div>
<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
	<div class="product-thumb">
		<div class="image">
				<img src="{{ asset('public/foodLogo/paket.png') }}" alt="image" title="image" class="img-responsive" />
			<div class="onhover1">
				<div class="button-group">
					<a href="{{ route('detail.quota') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>
				</div>
			</div>
		</div>
		<div class="caption text-center">
			<h4><a href="#">Internet Quota</a></h4>
			<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Internet Quota')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
			Rp.@convert($item->minPrice)
		<?php endforeach; ?>
	</p>
</div>
</div>	
</div>
<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
	<div class="product-thumb">
		<div class="image">
				<img src="{{ asset('public/foodLogo/salt.png') }}" alt="image" title="image" class="img-responsive" />
			<div class="onhover1">
				<div class="button-group">
					<a href="{{ route('detail.salt') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>
				</div>
			</div>
		</div>
		<div class="caption text-center">
			<h4><a href="#">Salt</a></h4>
			<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Salt')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
			Rp.@convert($item->minPrice)
		<?php endforeach; ?>
	</p>
</div>
</div>	
</div>
<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
	<div class="product-thumb">
		<div class="image">
				<img src="{{ asset('public/foodLogo/eggs.png') }}" alt="image" title="image" class="img-responsive" />
			<div class="onhover1">
				<div class="button-group">
					<a href="{{ route('detail.eggs') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>
				</div>
			</div>
		</div>
		<div class="caption text-center">
			<h4><a href="#">Egg's</a></h4>
			<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Eggs')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
			Rp.@convert($item->minPrice)
		<?php endforeach; ?>
	</p>
</div>
</div>	
</div>
<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
	<div class="product-thumb">
		<div class="image">
				<img src="{{ asset('public/foodLogo/meatt.png') }}" alt="image" title="image" class="img-responsive" />
			<div class="onhover1">
				<div class="button-group">
					<a href="{{ route('detail.meat') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>
				</div>
			</div>
		</div>
		<div class="caption text-center">
			<h4><a href="#">Meat</a></h4>
			<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Meat')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
			Rp.@convert($item->minPrice)
		<?php endforeach; ?>
	</p>
</div>
</div>	
</div>
<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
	<div class="product-thumb">
		<div class="image">
				<img src="{{ asset('public/foodLogo/milk.png') }}" alt="image" title="image" class="img-responsive" />
			<div class="onhover1">
				<div class="button-group">
					<a href="{{ route('detail.milk') }}"><button class="icons" type="button"><i class="icon_cart_alt"></i></button></a>
				</div>
			</div>
		</div>
		<div class="caption text-center">
			<h4><a href="#">Milk</a></h4>
			<p class="price">Start From <?php $data = App\Model\BasicNeed::where('type','=','Milk')->select(DB::raw('MIN(price) AS minPrice'))->get(); foreach ($data as $item) : ?>
			Rp.@convert($item->minPrice)
		<?php endforeach; ?>
	</p>
</div>
</div>	
</div>
</div>
</div>
@endsection

@section('bestdeal')
<div class="row">
	<div class="commontop text-center">
		<h4>Registered Store</h4>
	</div>
	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 bestdeal">
			<div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
				<img src="{{ asset('public/images/images.png') }}" style="margin-top: -40px" alt="image" title="image" class="img-responsive" />
			</div>
			<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
				<div class="box">
					<h3>Store</h3>
					<p>This is a Page for to show you a Registered Store.</p>
					<ul class="list-inline">
						<li><?php $data = App\Model\StoreAdmin::all(); if (count($data) < 10): ?>
						<div class="bg">0<?php $data = App\Model\StoreAdmin::all(); echo count($data) ?></div>Store
						<?php else: ?>
							<div class="bg"><?php $data = App\Model\StoreAdmin::all(); echo count($data) ?></div>Store
						<?php endif ?>

					</li>

				</ul>
				<hr>
				<a href="{{ route('store.user') }}"><button type="button">FIND STORE NOW</button></a>
			</div>	
		</div>
	</div>
</div>
</div>
@endsection

@section('mixveg')
<div class="container">
	<div class="row">
		<?php $data = App\Model\StoreAdmin::take(8)->get(); foreach ($data as $item): ?>

		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<div class="product-thumb1">
				<div class="image">
					<a href="#"><img src="{{ asset('public/storeLogo/'.$item->image) }}" alt="image" title="image" style="display: block; margin-left: auto;
					margin-right: auto;
					" width="60" height="60"	 /></a>
				</div>
				<div class="caption">
					<h4>{{ $item->name }}</h4>
					<p>Email : <span>{{ $item->email }}</span></p>
					<div class="button-group">
						<a href="/detail/store/{{ $item->id }}"><button type="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></i></button></a>
					</div>
				</div>
			</div>	
		</div>
	<?php endforeach ?>

</div>
</div>
<!-- Scripts -->
<script src="{{ asset('/dist/js/jquery.2.1.1.min.js') }}"></script>

@endsection