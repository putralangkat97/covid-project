@extends('layouts.master')

@section('title')
    Basic Needs
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Basic Need</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Basic Need</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
    </section>
    <div class="card">
        <div class="card-header">
          <a class="btn btn-icons btn-success float-right" href="javascript:void(0)" id="createNewItem"> <i class="fa fa-plus" style="color: white;"></i> </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="user_table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Item Name</th>
                <th>Value</th>
                <th>Description</th>
                <th>Price</th>
                <th>Item Type</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>

      <!-- Modal -->
  <div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="formStaffLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modelHeading"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" id="listform" method="POST" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" id="id">  
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Item Name</label>
              <div class="col-lg-9">
                <input type="text" name="item_name" id="item_name" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Value</label>
              <div class="col-lg-9">
                <input type="number" name="value" id="value" class="form-control" required="">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Price</label>
              <div class="col-lg-9">
                <input type="number" name="price" id="price" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Description</label>
              <div class="col-lg-9">
                <textarea rows="3" cols="3" id="description" name="description" class="form-control"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-form-label col-lg-3" style="font-size: 15px;">Item Type</label>
              <div class="col-lg-9">
                <select class="custom-select" name="type" id="type">
                  <option value="Rice">Rice</option>
                  <option value="Cooking Oil">Cooking Oil</option>
                  <option value="Milk">Milk</option>
                  <option value="Meat">Meat</option>
                  <option value="Kerosene">Kerosene</option>
                  <option value="Sugar">Sugar</option>
                  <option value="Salt">Salt</option>
                  <option value="Internet Quota">Internet Quota</option>
                  <option value="Eggs">Eggs</option>

                  
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success" id="saveBtn">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Delete Sweet Alert -->
  <div class="modal fade" role="dialog"  tabindex="-1" id="confirmationModal" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <img src="/public/images/sweetalert.png" alt="alert" style="margin-left:39%; margin-bottom: 0px; margin-top: 0px">
          <h3 class="text-center" style="margin-top:20px;">Are you Sure ?</h3>
          <p class="text-center" style="margin-top:10px; margin-bottom:0px;">Data cannot be returned</p>
        </div>
        <div class="modal-footer" style="margin-right:30%; margin-top:-10px;">
          <button type="button" name="ok_button" id="ok_button" class="btn btn-success">Yes, Delete</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div> 

      {{-- Script --}}
      <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
      <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
      <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
      <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
      <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
      <script src="{{ asset('js/sweetalert.min.js') }}"></script>
      <script>
           $(document).ready( function () {
     $.ajaxSetup({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
     $('#user_table').DataTable({
      destroy:true,
      processing: true,
      serverSide: true,
      paging: true,
      lengthChange: true,
      searching: true,
      ordering: true,
      info: false,
      autoWidth: false,
      responsive: true,
      ajax :{
        url: "{{ route('admin.need') }}",
        type: "GET",
      },
      columns: [
      { data: 'item', name: 'item' },
      { data: 'value', name: 'value' },
      { data: 'description', name: 'description' },
      { data: 'price', name: 'price',render: $.fn.dataTable.render.number( '.', ',', 2, 'Rp.' ) },
      { data: 'type', name: 'type',render:function(data){
       if (data=="Rice") {
        return '<span class="badge badge-success">RICE</span>';
      }else if(data=="Cooking Oil"){
        return '<span class="badge badge-warning">COOKING OIL</span>';
      }
      else if(data=="Internet Quota"){
        return '<span class="badge badge-danger">INTERNET QUOTA</span>';
      }
      else if(data=="Sugar"){
        return '<span class="badge badge-dark">SUGAR</span>';
      }
      else if(data=="Meat"){
        return '<span class="badge badge-primary">MEAT</span>';
      }
      else if(data=="Milk"){
        return '<span class="badge badge-info">MILK</span>';
      }
      else if(data=="Salt"){
        return '<span class="badge badge-dark">SALT</span>';
      }
      else if(data=="Eggs"){
        return '<span class="badge badge-primary">EGGS</span>';
      }
      else if(data=="Kerosene"){
        return '<span class="badge badge-success">KEROSENE</span>';
      }
    }},

    {data: 'action', name: 'action', orderable: false,searchable: false},
    ],
  });

  $('#createNewItem').click(function () {
      $('#saveBtn').val("create-product");
      $('#id').val('');
      $('#listform').trigger("reset");
      $('#modelHeading').html("<b>Add new Item</b>");
      $('#ajaxModal').modal('show');
    });

    $('#saveBtn').click(function (e) { 
      e.preventDefault(); 
      $(this).html('Saving...');  
      $.ajax({ 
        data: $('#listform').serialize(), 
        url: "{{ route('item.add') }}", 
        type: "POST", 
        dataType: 'json', 
        success: function (data) {  
          if (data.status == false) {
            swal("Oops!", data.description, "error");
          }else {
            swal("Success!", data.description, "success");
            $('#listform').trigger("reset"); 
            $('#modal').modal('hide'); 
            $('#saveBtn').html('Add'); 
            window.location.reload(); 


          } 

        } 
      });


           });
           $('body').on('click', '.updateItem', function () { 
      var staff_id = $(this).data('id'); 
      $.get("item/update/"+staff_id, function (data) { 
        $('#modelHeading').html("<b>Update Item</b>"); 
        $('#saveBtn').val("edit-user"); 
        $('#ajaxModal').modal('show'); 
        $('#id').val(data[0]['id']); 
        $('#item_name').val(data[0]['item']); 
        $('#type').val(data[0]['type']); 
        $('#price').val(data[0]['price']); 
        $('#value').val(data[0]['value']);  
        $('#description').val(data[0]['description']); 


      }) 
    });

    var id;
    $(document).on('click','.deleteItem',function(){
      id = $(this).data("id"); 
      console.log('id',id);
      $('#confirmationModal').modal('show');
    });

    $('#ok_button').click(function(){
      console.log('id',id);
      $.ajax({ 
        type: "DELETE", 
        url: "/item/delete/"+id, 
        data:{
          "_token":"{{ csrf_token() }}",
        },
        beforeSend:function(){
          $('#ok_button').text('Deleting...');
        },
        success: function (data) { 
          $('#confirmationModal').modal('hide');
          swal("Success", data.description, "success"); 
          $('#ok_button').text('Yes, Delete');

          location.reload(); 
        }, 
        error: function (data) { 
          console.log('Error:', data); 
        }  
      });  
    });

           });
           


      </script>
@endsection