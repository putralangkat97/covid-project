@extends('layouts.master')

@section('title')
    Contact Page
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
@endsection

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Contact Page</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Contact Page</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
    </section>
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
          <table id="user_table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Send Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- Delete Sweet Alert -->
  <div class="modal fade" role="dialog"  tabindex="-1" id="confirmationModal" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <img src="/public/images/sweetalert.png" alt="alert" style="margin-left:39%; margin-bottom: 0px; margin-top: 0px">
          <h3 class="text-center" style="margin-top:20px;">Are you Sure ?</h3>
          <p class="text-center" style="margin-top:10px; margin-bottom:0px;">Data cannot be returned</p>
        </div>
        <div class="modal-footer" style="margin-right:30%; margin-top:-10px;">
          <button type="button" name="ok_button" id="ok_button" class="btn btn-success">Yes, Delete</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div> 
    {{-- Script --}}
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready( function () {
     $.ajaxSetup({
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
     $('#user_table').DataTable({
      destroy:true,
      processing: true,
      serverSide: true,
      paging: true,
      lengthChange: true,
      searching: true,
      ordering: true,
      info: false,
      autoWidth: false,
      responsive: true,
      ajax :{
        url: "{{ route('admin.contact') }}",
        type: "GET",
      },
      columns: [
      { data: 'name', name: 'name' },
      { data: 'email', name: 'email' },
      { data: 'phone', name: 'phone' },
      { data: 'send_time', name: 'send_time' },
      { data: 'status', name: 'status',render:function(data){
       if (data=="Y") {
        return '<span class="badge badge-success">READ</span>';
      }else if(data=="N"){
        return '<span class="badge badge-danger">UNREAD</span>';
      }
      else if(data=="R"){
        return '<span class="badge badge-primary">REPLIED</span>';
      }
    }},

    {data: 'action', name: 'action', orderable: false,searchable: false},
    ],
  });
        });

        var id;
    $(document).on('click','.deleteEmail',function(){
      id = $(this).data("id"); 
      console.log('id',id);
      $('#confirmationModal').modal('show');
    });

    $('#ok_button').click(function(){
      console.log('id',id);
      $.ajax({ 
        type: "DELETE", 
        url: "/contact/delete/"+id, 
        data:{
          "_token":"{{ csrf_token() }}",
        },
        beforeSend:function(){
          $('#ok_button').text('Deleting...');
        },
        success: function (data) { 
          $('#confirmationModal').modal('hide');
          swal("Success", data.description, "success"); 
          $('#ok_button').text('Yes, Delete');

          location.reload(); 
        }, 
        error: function (data) { 
          console.log('Error:', data); 
        }  
      });  
    });
    </script>
@endsection