@extends('layouts.master')

@section('title')
Dashboard
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
	
@endsection

@section('content')
@foreach($data as $dt)
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Dashboard</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">

					<!-- Profile Image -->
					<div class="card card-primary card-outline">
						<div class="card-body box-profile">
							<div class="text-center">
								<img class="rounded-circle"
								src="{{ asset('public/storeLogo/'.$dt->image) }}"
								alt="store profile picture" width="150" height="150">
							</div>

							<h3 class="profile-username text-center">{{ $dt->name }}</h3>

							<p class="text-muted text-center">Store</p>

						</div>
						<!-- /.card-body -->
					</div>
				</div>
				<!-- /.card -->
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-6">
									<h5 style="margin-top: 10px"><b>Detail Store</b></h5>
								</div>
								<div class="col-6">
									<a class="btn btn-icons btn-warning editStore float-right" id="editStore" data-id="{{ $dt->id }}" href="javascript:void(0)"> <i class="fa fa-pencil-alt" style="color: white;"></i> </a>
								</div>
							</div>
							
						</div><!-- /.card-header -->
						<div class="card-body">
							<div class="tab-content">
								<div class="active tab-pane" id="activity">
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Store Name</b> </div>
										<div class="col-6"> {{ $dt->name }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Email</b> </div>
										<div class="col-6"> {{ $dt->email }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Address </b> </div>
										<div class="col-6"> {{ $dt->address }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Phone Number</b> </div>
										<div class="col-6"> {{ $dt->phone }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Description</b> </div>
										@if($dt->description == null)
										<div class="col-6 text-danger">Belum di input</div>
										@else
										<div class="col-6"> {{ $dt->description }}</div>
										@endif
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Open Time</b> </div>
										<div class="col-6"> {{ $dt->open_time }}</div>
									</div>
									<div class="row p-2 mb-2 bg-light text-dark">
										<div class="col-6  text-primary"><b>Closed Time</b> </div>
										<div class="col-6"> {{ $dt->closed_time }}</div>
									</div>


								</div>
								
							</div>
							<!-- /.tab-content -->
						</div><!-- /.card-body -->
					</div>
					<!-- /.nav-tabs-custom -->
				</div>
			</div>
		</div>
	</section>
	<!-- Modal -->
	<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="formStaffLabel" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modelHeading"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="" id="listform" method="POST" enctype="multipart/form-data">
						@csrf

						<input type="hidden" name="id" id="id">  
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="font-size: 15px;">Store Name</label>
							<div class="col-lg-9">
								<input type="text" name="store_name" id="store_name" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="font-size: 15px;">Email</label>
							<div class="col-lg-9">
								<input type="email" name="Email" id="email" class="form-control" required="">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="font-size: 15px;">Address</label>
							<div class="col-lg-9">
								<textarea rows="3" cols="3" id="address" name="Address" class="form-control"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="font-size: 15px;">Phone Number</label>
							<div class="col-lg-9">
								<input type="number" id="telp" name="Phone_Number" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="font-size: 15px;">Description</label>
							<div class="col-lg-9">
								<textarea rows="3" cols="3" id="description" name="description" class="form-control"></textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="font-size: 15px;">Store Image</label>
							<div class="col-lg-9">
								<input type="file" accept="image/*" name="image" id="image-preview">
								<input type="text" class="form-control" name="gambar" id="gambar-id" hidden>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3" style="font-size: 15px;">Open Time</label>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-5">
										<input class="form-control" type="time" id="open_time" name="open_time">
									</div>
									<label for="" class="col-form-label" style="font-size: 15px;">to</label>
									<div class="col-5">
										<input class="form-control" type="time" id="closed_time" name="closed_time">
									</div>

								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-success" id="saveBtn">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@endforeach
	<!-- Script -->
	<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>

	<script>
		$(document).ready( function () {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$('#saveBtn').click(function (e) { 
				e.preventDefault(); 
				$(this).html('Updating...');  
				$.ajax({ 
					data: $('#listform').serialize(), 
					url: "{{ route('admin.update') }}", 
					type: "POST", 
					dataType: 'json', 
					success: function (data) {  
						if (data.status == false) {
							swal("Oops!", data.description, "error");
						}else {
							swal("Success!", data.description, "success");
							$('#listform').trigger("reset"); 
							$('#modal').modal('hide'); 
							$('#saveBtn').html('Add'); 
							window.location.reload(); 


						} 

					} 
				});
			});

			$('body').on('click', '.editStore', function () { 
				var id = $(this).data('id'); 
				$.ajax({
					url:'editStore/'+ id,
					datatype: 'json',
					success: function(data){
					$('#modelHeading').html("<b>Update Store</b>"); 
					$('#ajaxModal').modal('show'); 
					$('#id').val(data[0]['id']); 
					$('#store_name').val(data[0]['name']); 
					$('#address').val(data[0]['address']); 
					$('#telp').val(data[0]['phone']); 
					$('#email').val(data[0]['email']); 
					$('#open_time').val(data[0]['open_time']); 
					$('#closed_time').val(data[0]['closed_time']);  
					$('#gambar-id').val(data[0]['image']);   
					$('#description').val(data[0]['description']); 
					
					}
				})
				
			});
			
	$('#image-preview').change(function () {
      if ($(this).val() != '') { 
        upload(this);
      }else{ 
      }
    });

     function upload(img) {
      var form = $('#listform')[0];
      var form_data = new FormData(form);
      form_data.append('image', img.files[0]);
      form_data.append('_token', '{{csrf_token()}}');
      $.ajax({
        url: "/store/uploadImage",
        data: form_data,
        type: 'POST',
        contentType: false,
        processData: false,
        success: function (data) {
          if (data.fail) { 
            alert(data.errors['image']);
          }
          else {
            console.log('gambar', data)
            $('#gambar-id').val(data); 
          }        },
          error: function (xhr, status, error) {
            alert(xhr.responseText); 
          }
        });
    }
		});
	</script>

	@endsection