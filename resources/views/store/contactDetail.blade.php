@extends('layouts.master')

@section('title')
Contact Detail
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">

@endsection

@section('content')
@foreach ($data as $item)

<div class="content-wrapper">
<section class="content-header">
<div class="container-fluid">
<div class="row mb-2">
<div class="col-sm-6">
<h1>Contact Detail</h1>
</div>
<div class="col-sm-6">
<ol class="breadcrumb float-sm-right">
<li class="breadcrumb-item"><a href="#">Contact</a></li>
<li class="breadcrumb-item active">Contact Detail</li>
</ol>
</div>
</div>
</div><!-- /.container-fluid -->
</section>

<section class="content">

<!-- Default box -->
<div class="card">
<div class="card-body">
<div class="row">
<div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
<div class="row">
<div class="col-12">
<h4>Description</h4>
<div class="post">
<div class="user-block">
    <span class="username">
    <a href="#">{{ $item->name }}.</a>
    </span>
    <span class="description">{{ $item->email }}</span>
</div>
<!-- /.user-block -->
<p>
    {{ $item->description }}
</p>

<p>
</p>
</div>
</div>
</div>
</div>
<div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
<h3 class="text-primary"><i class="fas fa-paint-brush"></i>&nbsp;User Profile</h3>
<br>
<div class="text-muted">
<p class="text-sm">User Name
<b class="d-block">{{ $item->name }}</b>
</p>
<p class="text-sm">User Email
<b class="d-block">{{ $item->email }}</b>
</p>
<p class="text-sm">Phone Number
<b class="d-block">{{ $item->phone }}</b>
</p><p class="text-sm">Contact Date
<b class="d-block">{{ $item->send_time }}</b>
</p>
</div>

<div class="text-center mt-5 mb-3">
<a href="{{ route('admin.contact') }}" class="btn btn-sm btn-danger">Back</a>
<a href="#" class="btn btn-sm btn-primary"  href="javascript:void(0)" id="SendingEmail">Reply</a>
</div>
</div>
</div>
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->

</section>
{{-- Reply --}}
<!-- Modal -->
<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog" aria-labelledby="formStaffLabel" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modelHeading"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <form action="" id="listform" method="POST" enctype="multipart/form-data">
        @csrf

        <input type="hidden" name="id" id="id">  
        <div class="form-group row">
            <label class="col-form-label col-lg-3" style="font-size: 15px;">User Name</label>
            <div class="col-lg-9">
            <input type="text" name="user_name" id="user_name" value="{{ $item->name }}" class="form-control" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-lg-3" style="font-size: 15px;">User Email</label>
            <div class="col-lg-9">
            <input type="email" name="user_email" id="user_email" value="{{ $item->email }}" class="form-control" required="" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-lg-3" style="font-size: 15px;">Your Email</label>
            <div class="col-lg-9">
            <input type="email" name="your_email" id="your_email" value="{{ Auth::user()->email }}" class="form-control" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-lg-3" style="font-size: 15px;">Message</label>
            <div class="col-lg-9">
            <textarea rows="3" cols="3" id="message" name="message" class="form-control"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success" id="saveBtn">Send</button>
        </div>
        </form>
    </div>
    </div>
</div>
</div>



@endforeach
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>


<script>
    $('#SendingEmail').click(function () {
    $('#saveBtn').val("create-product");
    $('#id').val('');
    $('#listform').trigger("reset");
    $('#modelHeading').html("<b>Reply Message</b>");
    $('#ajaxModal').modal('show');
});

$('#saveBtn').click(function (e) { 
      e.preventDefault(); 
      $(this).html('Sending...');  
      $.ajax({ 
        data: $('#listform').serialize(), 
        url: "{{ route('contact.send') }}", 
        type: "POST", 
        dataType: 'json', 
        success: function (data) {  
          if (data.status == false) {
            swal("Oops!", data.description, "error");
          }else {
            swal("Success!", data.description, "success");
            $('#listform').trigger("reset"); 
            $('#modal').modal('hide'); 
            $('#saveBtn').html('Add'); 
            window.location.reload(); 
     } 
    } 
  });
 });

</script>

@endsection