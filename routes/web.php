<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect('/login');
// });

// Auth::routes();
// Authentication
Route::redirect('/', '/ceksembakohome');
Route::GET('login', 'Auth\LoginController@show')->name('show');
Route::POST('login', 'Auth\LoginController@login')->name('login');
Route::GET('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth' => 'auth:superadmin']], function () {
    Route::GET('/dashboard', 'SuperadminController@dashboard')->name('dashboard');

    // Store
    Route::GET('/storeindex', 'SuperadminController@storeindex')->name('store.index');
    Route::POST('/store/add', 'SuperadminController@storeAdd')->name('store.add');
    Route::POST('/store/uploadImage', 'SuperadminController@uploadImage')->name('store.image');
    Route::GET('/store/update/{id}', 'SuperadminController@storeEdit')->name('store.edit');
    Route::GET('/store/detail/{id}', 'SuperadminController@storeDetail')->name('store.detail');
    Route::DELETE('store/delete/{id}', 'SuperadminController@deleteStore')->name('store.delete');

    // Store Log
    Route::GET('/storelog', 'SuperadminController@storeLOg')->name('store.log');

    // Contact Us
Route::GET('admin/contactus','SuperadminController@contactUs')->name('superadmin.contact')
;
Route::GET('admin/detailContact/{id}','SuperadminController@detailContact')->name('detail.contact');
Route::POST('admin/send','SuperadminController@adminSend')->name('admin.send');
Route::DELETE('admin/delete/{id}','SuperadminController@adminDelete')->name('admin.delete');
});

Route::group(['middleware' => ['auth' => 'auth:admin']], function () {
    Route::GET('/store/dashboard', 'StoreAdminController@dashboard')->name('admin.dashboard');
    Route::GET('/store/editStore/{id}', 'StoreAdminController@editStore')->name('admin.edit');
    Route::POST('store/updateAdmin', 'StoreAdminController@updateStore')->name('admin.update');
    Route::POST('/store/uploadImage', 'StoreAdminController@uploadImage')->name('store.image');

    // Basic Need
    Route::GET('/store/basicneed', 'StoreAdminController@basicNeed')->name('admin.need');
    Route::POST('store/itemadd','StoreAdminController@itemAdd')->name('item.add');
    Route::GET('store/item/update/{id}','StoreAdminController@itemUpdate')->name('item.update');
    Route::DELETE('item/delete/{id}','StoreAdminController@itemDelete')->name('item.delete');

    // Contact Page
    Route::GET('store/contact','StoreAdminController@contactIndex')->name('admin.contact');
Route::GET('store/detailContact/{id}','StoreAdminController@contactDetail')->name('admin.detail');
    Route::POST('store/contact/send','StoreAdminController@sendEmail')->name('contact.send');
    ROUTE::DELETE('contact/delete/{id}','StoreAdminController@messageDelete')->name('contact.delete');

});
// User
Route::GET('ceksembakohome','UserController@index')->name('user.index');
Route::GET('store','UserController@storeIndex')->name('store.user');
Route::GET('detail/store/{id}','UserController@storeDetail')->name('detail.store');
Route::GET('showdata/{id}','UserController@showData')->name('show.data');
Route::GET('/aboutus','UserController@aboutUs')->name('about.user');
Route::GET('/contactus','UserController@contactUs')->name('contact.user');

Route::POST('message/send','UserController@messageSend')->name('message.send');
Route::POST('superadmin/send','UserController@userSend')->name('user.send');
// Foods
Route::GET('detail/rice','UserController@riceDetail')->name('detail.rice');
Route::GET('detail/cookingOil','UserController@oilDetail')->name('detail.oil');
Route::GET('detail/sugar','UserController@sugarDetail')->name('detail.sugar');
Route::GET('detail/salt','UserController@saltDetail')->name('detail.salt');
Route::GET('detail/quota','UserController@quotaDetail')->name('detail.quota');
Route::GET('detail/milk','UserController@milkDetail')->name('detail.milk');
Route::GET('detail/meat','UserController@meatDetail')->name('detail.meat');
Route::GET('detail/eggs','UserController@eggsDetail')->name('detail.eggs');





